defmodule Yahtzee do
  
  def score_upper(dice, type) do
    cond do
      type == 1 ->
        %{Ones: length(Enum.filter(dice, fn e -> e == 1 end))}
      type == 2 ->
        %{Twos: length(Enum.filter(dice, fn e -> e == 2 end))}
      type == 3 ->
        %{Threes: length(Enum.filter(dice, fn e -> e == 3 end))}
      type == 4 ->
        %{Fours: length(Enum.filter(dice, fn e -> e == 4 end))}
      type == 5 ->
        %{Fives: length(Enum.filter(dice, fn e -> e == 5 end))}
      type == 6 ->  
        %{Sixes: length(Enum.filter(dice, fn e -> e == 6 end))}
      end
  end
  
  def score_lower(dice, type) do
    cond do
      type == 3 ->
        %{"Three of a kind": Enum.sum(dice)}
      type == 4 ->
        %{"Four of a kind": Enum.sum(dice)}
      type == FH ->
        dice = Enum.sum(dice)
        x = 25 - dice
        dice = dice + x
        %{"Full house": dice}
      type == Y ->
        dice = Enum.sum(dice)
        x = 50 - dice
        dice = dice + x
        %{"Yahtzee": dice}
      type == C ->
        %{"Chance": Enum.sum(dice)}  
    end
  end
 
  def score_lowerC(dice) do
    dice = Enum.sum(dice)
    x = 10 - dice
    dice = dice + x
    %{"RandomCase": dice}    
  end
end
