defmodule YahtzeeLowerSectionTest do
    use ExUnit.Case
    doctest Yahtzee
  
    def generate(dice_face, occurrences) do
      Enum.to_list(1..6)
      |> List.delete(dice_face)
      |> Enum.shuffle
      |> Enum.take(5 - occurrences)
      |> Enum.concat(List.duplicate(dice_face, occurrences))
      |> Enum.shuffle
    end
  
    test "Identify 'Three of a kind' with ones" do
      dices = generate(1, 3)
      sum = Enum.sum(dices)
      assert %{"Three of a kind": ^sum} = Yahtzee.score_lower(dices,3) 
    end
  
    test "Identify 'Three of a kind' with all the others" do
      Enum.map(2..6, fn (dice_face) ->
        dices = generate(dice_face, 3)
        sum = Enum.sum(dices)
        assert %{"Three of a kind": ^sum} = Yahtzee.score_lower(dices,3)
    end)
    end

test "Identify 'Four of a kind' with every face" do
    Enum.map(1..6, fn (dice_face) ->
      dices = generate(dice_face, 4)
      sum = Enum.sum(dices)
      assert %{"Four of a kind": ^sum} = Yahtzee.score_lower(dices,4)
    end)
end

test "Identify 'Full house' with every face" do
    Enum.map(1..6, fn _ ->
      [x,y] =
        Enum.shuffle(1..6) #Random order
        |> Enum.take(2) #Toma dos valores del inicio
      assert %{"Full house": 25} = Yahtzee.score_lower([x,x,x,y,y] |> Enum.shuffle, FH)
    end)
  end
#   Parte 4
#   test "Identify 'Four of a kind' with every face" do
#     Enum.map(1..6, fn (dice_face) ->
#       dices = generate(dice_face, 4)
#       sum = Enum.sum(dices)
#       assert %{"Four of a kind": ^sum} = Yahtzee.score_lower(dices)
#     end)
#   end
#   test "Identify 'Full house' with every face" do
#     Enum.map(1..6, fn _ ->
#       [x,y] =
#         Enum.shuffle(1..6)
#         |> Enum.take(2)
#       assert %{"Full house": 25} = Yahtzee.score_lower([x,x,x,y,y] |> Enum.shuffle)
#     end)
#   end
test "Identify 'Yahtzee'" do
    Enum.map(1..6, fn n ->
      assert %{Yahtzee: 50} = Yahtzee.score_lower(List.duplicate(n,5), Y)
    end)
  end
  test "Identify any other combination" do
    Enum.map(1..6, fn _ ->
      [x,y,z] =
        Enum.shuffle(1..6)
        |> Enum.take(3)
      seq = Enum.shuffle([x,x,y,y,z])
      sum = Enum.sum(seq)
      assert %{Chance: ^sum} = Yahtzee.score_lower(seq,C)
    end)
  end

  test "Identify a safety case" do
    Enum.map(1..6, fn _ ->
      [x,y,z] = [2,4,6]
      assert %{"RandomCase": 10} = Yahtzee.score_lowerC([x,x,y,z,z] |> Enum.shuffle)
    end)
  end

  test "Random Case" do
    Enum.map(1..6, fn _ ->
      [x,y] =
        Enum.shuffle(1..6) 
        |> Enum.take(3) 
      assert %{"RandomCase": 10} = Yahtzee.score_lowerC([x,x,y,z,z] |> Enum.shuffle)
    end)
  end

end